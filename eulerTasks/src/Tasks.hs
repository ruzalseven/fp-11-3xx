module Tasks
	(
    getCount,
    getQ,
    get
  ) where
sg = [1, 1, -1, -1]
getCount :: Integer -> Integer -> [Integer] -> Integer -> [Integer]
getCount n i p px | n <= 0 = [0]
                  | otherwise =(if ((getQ !! (fromIntegral i)) <= n)
                    then (getCount n (i+1) p (px + (p!!(fromIntegral (n-(getQ !! (fromIntegral i)))) * (sg!!(fromIntegral (i `mod` 4))))))
                    else p ++ [px `mod` (10^9) ])

getQ  = foldl (++) [] [((x*(3*x-1) `quot` 2) : ((x*(3*x-1) `quot` 2)+x) : []) | x <- [1..250]]

get :: Integer -> Integer -> [Integer] ->Integer
get n r p = if ((res!!(fromIntegral n)) `mod` r == 0) then n
            else get (n+1) r res
  where
    res = getCount n 0 p 0
