-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HW0"
            [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ]
          , testGroup "HW2"
            [ testGroup "isKnown"
              [ testCase "isKnown On" $ isKnown On @?= True
              , testCase "isKnown Off" $ isKnown Off @?= True
              , testCase "isKnown Unknown" $ isKnown Unknown @?= False
              ]
            , testGroup "eval"
              [ testCase "eval 1" $ eval (Const 1) @?= 1
              , testCase "eval 1 + 2" $ eval (Add (Const 1) (Const 2)) @?= 3
              , testCase "eval -5 + 3" $ eval (Add (Const $ -5) (Const 3)) @?= -2
              , testCase "eval 3 - 1" $ eval (Sub (Const 3) (Const 1)) @?= 2
              , testCase "eval -3 - 1" $ eval (Sub (Const $ -3) (Const 1)) @?= -4
              , testCase "eval 3 * 2" $ eval (Mult (Const 3) (Const 2)) @?= 6
              , testCase "eval -3 * 2" $ eval (Mult (Const $ -3) (Const 2)) @?= -6
              , testCase "eval -3 * -2" $ eval (Mult (Const $ -3) (Const $ -2)) @?= 6
              , testCase "eval (-3 - -2) * 5 - (1 - 0)" $ eval (Sub (Mult (Sub (Const $ -3) (Const $ -2)) (Const 5)) (Sub (Const 1) (Const 0))) @?= -6
              , testCase "eval (1 + 2) * (3 + 1)" $ eval (Mult (Add (Const 1) (Const 2)) (Add (Const 3) (Const 1))) @?= 12
              , testCase "eval 5*1*6 + 5*2*6" $ eval (Add (Mult (Mult (Const 1) (Const 5)) (Const 6)) (Mult (Mult (Const 2) (Const 5)) (Const 6))) @?= 90
              ]
            , testGroup "simplify"
              [ testCase "simplify (1 + 2) * 3" $ simplify (Mult (Add (Const 1) (Const 2)) (Const 3)) @?= Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
              ,testCase "simplify (1 + 2) * (3 + 1)" $ simplify (Mult (Add (Const 1) (Const 2)) (Add (Const 3) (Const 1))) @?= Add (Add (Mult (Const 1) (Const 3)) (Mult (Const 1) (Const 1))) (Add (Mult (Const 2) (Const 3)) (Mult (Const 2) (Const 1)))
              ,testCase "simplify (5*(1+2))*6" $ simplify (Mult (Mult (Add (Const 1) (Const 2)) (Const 5)) (Const 6)) @?= Add (Mult (Mult (Const 1) (Const 5)) (Const 6)) (Mult (Mult (Const 2) (Const 5)) (Const 6))
              ]
            ]
          ]