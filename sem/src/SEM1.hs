module SEM1
       ( eval1,
        eval,
        renameVar,
        renameVars,
       Term (..),
       replaceVars
       ) where

data Term = Var String
            |Lambda String Term
            |Apply Term Term
            deriving(Eq)
instance Show Term where
    show (Var v) = v
    show (Lambda v t) = "(\\" ++ show v ++ " . " ++ show t ++ ")"
    show (Apply t1 t2) = "("++ show t1 ++ " " ++ show t2 ++ ")"

eval1 :: Term -> Maybe Term
eval1 (Var x) = Just (Var x)
eval1 (Apply (Var x) (Var y))=Just (Apply (Var x) (Var y))
eval1 (Apply (Lambda x y) t) =  case result of
      (Apply x y) -> eval1 (Apply x y)
      otherwise -> Just result
      where
        result = (replaceVars x [] t y)
eval1 (Apply (Apply x y) m) = case result of
      (Just t) -> eval1 (Apply t m)
      otherwise -> Nothing
      where
        result = eval1 $ Apply x y
eval1 (Apply (Lambda x y) t) = Just $ replaceVars x [] t y
eval1 t = Nothing

eval :: Term -> Term
eval x = case result of
  (Just x) -> x
  otherwise -> Var "Not evaulated"
  where
    result = eval1 x
replaceVars :: String -> [String] -> Term -> Term -> Term
replaceVars x l t (Var y) = if x==y then (renameVars l t) else  (Var y)
replaceVars x l t (Lambda y z) = Lambda y (replaceVars x (y:l) t z)
replaceVars x l t (Apply y z) = Apply (replaceVars x l t y) (replaceVars x l t z)

renameVar :: [String] -> String -> Int -> String
renameVar lst x n | elem (x ++ (show n)) lst = renameVar lst x (n+1)
                       | otherwise = x ++ (show n)

renameVars :: [String] -> Term -> Term
renameVars lst (Lambda x term) | elem x lst = Lambda x (renameVars (filter (\el -> el /= x) lst) term)
                               | otherwise = Lambda x (renameVars lst term)
renameVars lst (Apply t1 t2) = Apply (renameVars lst t1) (renameVars lst t2)
renameVars lst (Var x) | elem x lst = Var (renameVar lst x 0)
                       | otherwise = Var x
